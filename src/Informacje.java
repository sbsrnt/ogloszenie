public class Informacje implements Comparable<Informacje>{
	public String markaSamochodu;
	public int cena;
	public int przebieg;
	public int rocznik;
	public String region;
	public String miasto;
	public String czyUszkodzony;
	public String czyNowy;
	
	public Informacje(String markaSamochodu, int cena, int przebieg, int rocznik, String region,  String miasto,  String czyUszkodzony, String czyNowy){
        this.markaSamochodu = markaSamochodu;
        this.cena = cena;
        this.przebieg = przebieg;
        this.rocznik = rocznik;
        this.region = region;
        this.miasto = miasto;
        this.czyUszkodzony = czyUszkodzony;
        this.czyNowy = czyNowy;
        
    }
	
	
	public int compareTo(Informacje i) {
        return markaSamochodu.compareTo(i.pobierzMarkeSamochodu());
    }
	
	public void markaSamochodu(String markaSamochodu)
    {
        this.markaSamochodu = markaSamochodu;
    }
	
	public void cena(int cena)
    {
        this.cena = cena;
    }
	
	public void przebieg(int przebieg)
    {
        this.przebieg = przebieg;
    }
	
	public void rocznik(int rocznik)
    {
        this.rocznik = rocznik;
    }
	
	public void region(String region)
    {
        this.region = region;
    }
	
	public void miasto(String miasto)
    {
        this.miasto = miasto;
    }
	
	public void czyUszkodzony(String czyUszkodzony)
    {
        this.czyUszkodzony = czyUszkodzony;
    }
	
	public void czyNowy(String czyNowy)
    {
        this.czyNowy = czyNowy;
    }
	
	public String pobierzMarkeSamochodu()
	{
		return markaSamochodu;
	}
	
	public int pobierzCene()
	{
		return cena;
	}
	
	public int pobierzPrzebieg()
	{
		return przebieg;
	}
	
	public int pobierzRocznik()
	{
		return rocznik;
	}
	
	public String pobierzRegion()
	{
		return region;
	}
	
	public String pobierzMiasto()
	{
		return miasto;
	}
	
	public String pobierzCzyUszkodzony()
	{
		return czyUszkodzony;
	}
	
	public String pobierzCzyNowy()
	{
		return czyNowy;
	}
    
}