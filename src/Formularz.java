import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

 class Formularz {
	private static List<Informacje> listaOgloszen = new ArrayList<Informacje>();
	
	
	public static void main(String[] args) {
		dodajOgloszenie();
		wyswietlListeWgModeli(null);
		wyswietlListeWgPrzebiegu(null);
		wyswietlListeWgRocznika(null);
		wyswietlListeWgCeny(null);
		startIO();

	}

	
	public static void startIO(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        while(true){            
            try {
                input = reader.readLine().toLowerCase();

                if(input.equals(""))
                {
                    continue;
                }
                else if (input.equals("q"))
                { 
                    System.exit(0); 
                }
                else if(Integer.parseInt(input) > 0 && Integer.parseInt(input) <= listaOgloszen.size())
                { 
                	wyswietlInformacjeOAucie(listaOgloszen.get(Integer.parseInt(input)-1));
                    break;
                }
                else
                {
                    throw new IOException();
                }
            } catch (Exception e) {
                System.out.println("Nieprawidłowy numer: " + input + "\n");
            }
        }   
    }
	
	
	public static void wyswietlListeWgModeli(Informacje informacje){
		int count = 1;
        System.out.println("Lista ogloszen do wyboru:");
 
        for (Informacje f : listaOgloszen){
            System.out.println(count++ +". " + f.pobierzMarkeSamochodu());
        }
    }
	
	
	
	public static void wyswietlInformacjeOAucie(Informacje informacje)
	{
		Date dataOgloszenia = new Date();
		Date dataDoZaplaty = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dataDoZaplaty); 
		int liczbaDni = 5 + (int)(Math.random() * ((100 - 5) + 1));
		c.add(Calendar.DATE, liczbaDni);
		dataOgloszenia = c.getTime();
		
             System.out.println("Wybrany model: " + informacje.pobierzMarkeSamochodu() + "\n"+
            					"Cena: " + informacje.pobierzCene()+ "zł \n"+
            					"Przebieg: " + informacje.pobierzPrzebieg()+ "km \n"+
            					"Rocznik: " + informacje.pobierzRocznik()+ " \n"+
            					"Miasto: " + informacje.pobierzMiasto() + " \n"+
            					"Czy uszkodzony? " + informacje.pobierzCzyUszkodzony() + "\n" +
            					"Czy nowy? " + informacje.pobierzCzyNowy() + "\n" +
            					"Data ogłoszenia: " + dataOgloszenia + "\n"
            		 			);
 
    }
	
	
	public static void dodajOgloszenie(Informacje i)
	 {
	        listaOgloszen.add(i);
	 }
	
	public static void dodajOgloszenie()
	{		
       dodajOgloszenie(new Informacje("Ford", 12000, 156845, 2005,"Pomorskie", "Gdańsk", "Tak", "Nie"));
       dodajOgloszenie(new Informacje("Mazda", 20410, 264854, 2007, "Lubelskie", "Lublin","Nie", "Nie"));
       dodajOgloszenie(new Informacje("BMW", 38500, 68458, 2009, "Mazowieckie", "Warszawa", "Nie", "Tak"));
       dodajOgloszenie(new Informacje("Mercedes", 11000, 35186, 2014, "Pomorskie", "Gdynia","Tak", "Tak"));
       dodajOgloszenie(new Informacje("Skoda", 5000, 359846, 1999, "Pomorskie", "Sopot" ,"Nie", "Nie"));
       Collections.sort(listaOgloszen);
   }
}
